// animated navbar js
$(window).scroll(function() {
    if ($("nav").offset().top > 50) {
        $("nav").addClass("black");
    } else {
        $("nav").removeClass("black");
    }
});


$( ".responsive" ).click(function() {
$( "nav ul" ).toggle( "slow" );
});


// progressbar js
$('.barra-nivel').each(function() {
    var valorLargura = $(this).data('nivel');
    var valorNivel = $(this).html("<span class='valor-nivel'>"+valorLargura+"</span>");

    $(this).animate({
        width: valorLargura
    });
});


// owl carousel 

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
})


// aos animation 

AOS.init();
